FROM golang:1.17-alpine AS build

ARG version="dev"
WORKDIR /app/

COPY go.* /app/
RUN go mod download

COPY . /app/
RUN go build -ldflags "-X main.Version=${version}" -o gitlab-package-proxy ./main.go

FROM alpine:3.14 AS deploy

COPY --from=build /app/gitlab-package-proxy /usr/local/bin/

CMD ["/usr/local/bin/gitlab-package-proxy"]
