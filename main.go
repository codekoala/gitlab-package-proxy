package main

import (
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/http"
	"os"
	"runtime"
	"time"

	"github.com/labstack/echo/v4"
)

// BaseURL is the primary endpoint for interacting with GitLab's API
const BaseURL = "https://gitlab.com/api/v4"

// Version represents the version of the utility
var Version = "dev"

var (
	// accessToken is a GitLab Personal Access Token, which is populated through the GITLAB_ACCESS_TOKEN environment
	// variable
	accessToken string

	// projectID is the ID of the GitLab project whose packages will be exposed
	projectID string

	// groupID is the ID of the GitLab group whose packages will be exposed
	groupID string

	// addr is the address to bind to when serving requests
	addr string

	client *http.Client
)

type (
	// Pipeline represents pertinent information about pipelines which produced a package
	Pipeline struct {
		ID  int64  `json:"id"`
		Ref string `json:"ref"`
	}

	// Package represents pertinent information about a project's package
	Package struct {
		ID          int64    `json:"id"`
		Name        string   `json:"name"`
		Version     string   `json:"version"`
		PackageType string   `json:"package_type"`
		Pipeline    Pipeline `json:"pipeline"`
	}
)

func init() {
	if accessToken = os.Getenv("GITLAB_ACCESS_TOKEN"); accessToken == "" {
		log.Fatal("GITLAB_ACCESS_TOKEN must be set")
	}

	if addr = os.Getenv("GITLAB_PACKAGE_PROXY_ADDR"); addr == "" {
		addr = ":1323"
	}

	if projectID = os.Getenv("GITLAB_PROXY_PROJECT_ID"); projectID != "" {
		log.Printf("Proxying requests for GitLab Project ID: %s", projectID)
	} else if groupID = os.Getenv("GITLAB_PROXY_GROUP_ID"); groupID != "" {
		log.Printf("Proxying requests for GitLab Group ID: %s", groupID)
	} else {
		log.Fatal("One of GITLAB_PROXY_PROJECT_ID or GITLAB_PROXY_GROUP_ID must be set")
	}
}

func main() {
	log.Printf("GitLab Package Proxy (version: %s; %s)", Version, runtime.Version())
	client = &http.Client{
		Timeout: time.Second * 10,
	}

	e := echo.New()
	e.Match([]string{"GET", "HEAD", "OPTIONS"}, "/_heartbeat", heartbeat)
	e.GET("/latest/:packageName/:fileName", latestPackageFile)
	e.GET("/*", proxyRequest)

	e.Logger.Fatal(e.Start(addr))
}

// heartbeat allows a simple health check to ensure the service is responsive
func heartbeat(c echo.Context) error {
	c.Response().Header().Set("X-Proxy-Version", Version)
	return c.NoContent(http.StatusOK)
}

// proxyRequest attempts to proxy all requests to GitLab with a PRIVATE-TOKEN header
func proxyRequest(c echo.Context) error {
	// method is hardcoded to GET to avoid potentially destructive abuse
	res, err := doRequest("GET", c.Request().URL.Path, nil)
	if err != nil {
		return fmt.Errorf("Got error %s", err.Error())
	}
	defer res.Body.Close()

	// return the API's response directly
	return c.Stream(res.StatusCode, res.Header.Get("Content-Type"), res.Body)
}

// latestPackageFile attempts to find and return the most recent file for the specified package, optionally filtering
// packages by the reference which kicked off a pipeline.
//
// GET /latest/:packageName/:fileName[?order_by=:orderBy&ref=:ref]
//
func latestPackageFile(c echo.Context) error {
	pkgName := c.Param("packageName")
	fileName := c.Param("fileName")
	ref := c.QueryParam("ref")
	orderBy := c.QueryParam("order_by")

	if pkgName == "" || fileName == "" {
		// this shouldn't happen if the router sent us here, but...
		return fmt.Errorf("must specify both a package name (got: %v) and a file name (got: %v) within the package", pkgName, fileName)
	}

	if orderBy == "" {
		// orderBy = "version"
		orderBy = "created_at"
	}

	// list packages, sorted with newest first
	path := fmt.Sprintf("?package_name=%s&order_by=%s&sort=desc", pkgName, orderBy)
	res, err := doRequest("GET", path, nil)
	if err != nil {
		return fmt.Errorf("got error %s", err.Error())
	}

	// read response from API
	defer res.Body.Close()
	body, readErr := ioutil.ReadAll(res.Body)
	if readErr != nil {
		return readErr
	}

	// unmarshal response from API
	var packages []Package
	if err := json.Unmarshal(body, &packages); err != nil {
		return err
	}

	// ensure we have at least one package
	if len(packages) <= 0 {
		return fmt.Errorf("no package %v found", pkgName)
	}

	// find the most recent package, optionally with matching pipeline ref
	var top Package
	for _, top = range packages {
		if ref != "" {
			if top.Pipeline.Ref == ref {
				break
			}
		} else {
			break
		}
	}

	// download the specified file from the most recent package
	res, err = doRequest("GET", fmt.Sprintf("/%s/%s/%s/%s", top.PackageType, top.Name, top.Version, fileName), nil)
	if err != nil {
		return err
	}
	defer res.Body.Close()
	return c.Stream(res.StatusCode, res.Header.Get(echo.HeaderContentType), res.Body)
}

// newRequest creates a new http.Request with the necessary credentials to access GitLab's API
func newRequest(method, url string, body io.Reader) (req *http.Request, err error) {
	if req, err = http.NewRequest(method, url, nil); err != nil {
		return nil, err
	}
	req.Header.Set("PRIVATE-TOKEN", accessToken)

	return req, nil
}

// doRequest issues an authenticated request to GitLab's API and returns the response to the caller.
func doRequest(method, path string, body io.Reader) (*http.Response, error) {
	var url string

	if projectID != "" {
		url = fmt.Sprintf("%s/projects/%s/packages%s", BaseURL, projectID, path)
	} else if groupID != "" {
		url = fmt.Sprintf("%s/groups/%s/packages%s", BaseURL, groupID, path)
	}
	log.Printf("Proxying request to %s => %s", path, url)
	req, err := newRequest(method, url, nil)
	if err != nil {
		return nil, err
	}

	return client.Do(req)
}
